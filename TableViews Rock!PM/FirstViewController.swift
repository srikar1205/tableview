//
//  FirstViewController.swift
//  TableViews Rock!PM
//
//  Created by Patle,Srikar on 2/19/19.
//  Copyright © 2019 Patle,Srikar. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
     var cities = ["Edinburgh", "London", "Aberdeen", "Oxford", "cambridge"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "city")!
        cell.textLabel?.text = cities[indexPath.row]
        return cell
       
    }
    
  
    

    

   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}



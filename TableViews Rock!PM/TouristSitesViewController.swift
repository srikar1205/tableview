//
//  SecondViewController.swift
//  TableViews Rock!PM
//
//  Created by Patle,Srikar on 2/19/19.
//  Copyright © 2019 Patle,Srikar. All rights reserved.
//

import UIKit

class TouristSitesViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    var tourists = ["new york", "chicago", "maryville", "carlifornia", "kansas"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tourists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "tourist")!
        cell.textLabel?.text = tourists[indexPath.row]
        return cell
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

